import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Home from './page/home';
import P01 from './page/p01';
import P02 from './page/p02';
import P03 from './page/p03';

function App() {
  return (
    <Router>
      <div className="App">
        <div className="menu">
          <ul>
            <li><Link to="/">home</Link></li>
            <li><Link to="/p01">p01</Link></li>
            <li><Link to="/p02">p02</Link></li>
            <li><Link to="/p03">p03</Link></li>
          </ul>
        </div>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/P01">
            <P01 />
          </Route>
          <Route path="/P02">
            <P02 />
          </Route>
          <Route path="/P03">
            <P03 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
